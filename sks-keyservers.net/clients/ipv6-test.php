<?php
	if(!isset($_GET['keyserver']) || !isset($_GET['ipv6']))
		die("Missing input data");

	$host = $_GET['keyserver']; //Set host to first argument
	$hostip = $_GET['ipv6'];

	$port=11371; //Manually set port

	$return_array = array();
	$return_array['client_server'] = "s01.sks-keyservers.net";
	$return_array['request_keyserver'] = $host;
	$return_array['request_ipv6'] = $hostip;

	$return_array['statusipv6ok'] = false;    
    
	$sock = socket_create(AF_INET6, SOCK_STREAM, SOL_TCP);    
	socket_set_option($sock, SOL_SOCKET, SO_SNDTIMEO, 
		array('sec' => 5, 'usec' => 0)); 
	if(@socket_connect($sock, $hostip, $port))    
		$return_array['statusipv6ok'] = true;    
    
	socket_close($sock);  

	echo json_encode($return_array);
?>
