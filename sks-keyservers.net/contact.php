<?
 /*
  *  contact.php
  *  Copyright (C) 2006, 2007, 2008, 2009, 2010, 2011, 2012  Kristian Fiskerstrand
  *  
  *  This file is part of SKS Keyserver Pool (http://sks-keyservers.net)
  *  
  *  The Author can be reached by electronic mail at kf@sumptuouscapital.com
  *  Communication using OpenPGP is preferred - a copy of the public key 0x0B7F8B60E3EDFAE3
  *  is available in all the common keyservers or in hkp://pool.sks-keyservers.net
  *  
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */
  
 $dir = "./";
 $title="Contact";
 include($dir."inc/header.inc.php");
?>
<p>If you want to contact me, use the email information found in the OpenPGP public keyblock
<a href="https://sks-keyservers.net/pks/lookup?op=get&amp;search=0x94CBAFDD30345109561835AA0B7F8B60E3EDFAE3">0x94CBAFDD30345109561835AA0B7F8B60E3EDFAE3</a>
</p>

<p>sks-keyservers.net is released under the GNU General Public License v3 and 
the sourcecode is available at 
<a href="https://git.sumptuouscapital.com/?p=sks-keyservers-pool.git;a=summary">git.sumptuouscapital.com</a>.
</p>
 
<?
 include($dir."/inc/footer.inc.php");
?>
