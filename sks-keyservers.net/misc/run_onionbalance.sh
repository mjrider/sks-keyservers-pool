#!/bin/bash
TIMEOUT_VALUE="6h"
R_DIR="/var/lib/onionbalance/config/master"

while [[ 1 ]]; do
	curl https://sks-keyservers.net/status/tor.php > ${R_DIR}/config.yaml
	timeout ${TIMEOUT_VALUE} onionbalance -c ${R_DIR}/config.yaml
done;
