<?
 /*
  *  status-srv/tor.php: The Onion Router balancing
  *  Copyright (C) 2015  Kristian Fiskerstrand
  *  
  *  This file is part of SKS Keyserver Pool (http://sks-keyservers.net)
  *  
  *  The Author can be reached by electronic mail at kf@sumptuouscapital.com
  *  Communication using OpenPGP is preferred - a copy of the public key 0x0B7F8B60E3EDFAE3
  *  is available in all the common keyservers or in hkp://pool.sks-keyservers.net
  *  
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */
  
 require("sks.inc.php");
 require("sks-status.inc.php");
 
 header("Content-type: text/plain");
 $servers = unserialize(file_get_contents(dirname(__FILE__)."/sks_cache.serialized"));
 $serverarr = $servers->get_servers();
 
 $status_collection = unserialize(file_get_contents(dirname(__FILE__)."/sks_cache_status_collection.serialized"));
 $servercolarr = $status_collection->get_servers();
 
 sort($serverarr);
 
 $array_of_all_online_servers = array();
 $array_of_selected_servers = array(); 
 
 // Server selection
 foreach($serverarr as $server)
 {
 	if(!(isset($servercolarr[$server->get_hostname()]['last_status']) && $servercolarr[$server->get_hostname()]['last_status']==1))
		continue;

 	if(!$server->get_is_reverse_proxy())
		continue;

	if(!$server->get_tor_addresse())
		continue; 

    $array_of_all_online_servers[] = array($server->get_hostname(), $server->get_tor_addresse());
 }
 
 // Select only 10 random servers
 if(count($array_of_all_online_servers)>10)
 {
  	$server_keys = array_rand($array_of_all_online_servers,10);
  	foreach($server_keys as $id)
  	{
  		$array_of_selected_servers[] = $array_of_all_online_servers[$id];
  	}
 }
 else
 {
 	$array_of_selected_servers = $array_of_all_online_servers; 
 }
 
 echo "# Auto-generated OnionBalance Config File\n";
 echo "# Updated: ".(date("Y-m-d H:i"))." UTC\n";
 echo "services:\n";
 echo "- instances:\n";

 foreach($array_of_selected_servers as $server)
 {
	$server_addr = substr($server[1], 0, strlen($server[1])-6);

	echo "  - address: {$server_addr}\n    name: {$server[0]}\n";	  
 }
 echo "  key: jirk5u4osbsr34t5.key\n";
?>
