<?
 /*
  *  contact.php
  *  Copyright (C) 2006, 2007, 2008, 2009, 2010, 2011, 2012  Kristian Fiskerstrand
  *  
  *  This file is part of SKS Keyserver Pool (http://sks-keyservers.net)
  *  
  *  The Author can be reached by electronic mail at kf@sumptuouscapital.com
  *  Communication using OpenPGP is preferred - a copy of the public key 0x0B7F8B60E3EDFAE3
  *  is available in all the common keyservers or in hkp://pool.sks-keyservers.net
  *  
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */
  
 $dir = "./";
 $title="Verify SSL/TLS certificate";
 include($dir."inc/header.inc.php");
?>
<h1>Validation of HTTPS connection</h1>
<p><a href="https://sks-keyservers.net">https://sks-keyservers.net</a> uses a certificate signed by COMODO, but for additional security it can also be authenticated through the OpenPGP Web of Trust by using <a href="http://web.monkeysphere.info">Monkeysphere</a></p> 

<h1>HKPS pool verification</h1>
<p>The HKPS pool only include servers that have been certified by the sks-keyservers.net CA, of which the certificate can be found at <a href="https://sks-keyservers.net/sks-keyservers.netCA.pem">https://sks-keyservers.net/sks-keyservers.netCA.pem</a>[<a href="https://sks-keyservers.net/sks-keyservers.netCA.pem.asc">OpenPGP signature</a>][<a href="https://sks-keyservers.net/ca/crl.pem">CRL</a>]. The fingerprint of this certificate is <b>79:1B:27:A3:8E:66:7F:80:27:81:4D:4E:68:E7:C4:78:A4:5D:5A:17</b> and the X509v3 Subject Key Identifier is <b>E4 C3 2A 09 14 67 D8 4D 52 12 4E 93 3C 13 E8 A0 8D DA B6 F3</b></p>
<p>More detailed information about how to use the HKPS pools is available in the <a href="/overview-of-pools.php#pool_hkps">overview of pools page</a></p>
<?
 include($dir."/inc/footer.inc.php");
?>
